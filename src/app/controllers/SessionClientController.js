import jwt from 'jsonwebtoken';
import * as Yup from 'yup';

import Client from '../models/Client';
import authConfig from '../../config/auth';

class SessionClientController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string().required()
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation failed' });
    }

    const { email, password } = req.body;

    const client = await Client.findOne({ where: { email } });

    if (!client) {
      return res
        .status(401)
        .json({ error: 'Email or password does not match' });
    }
    if (!(await client.checkPassword(password))) {
      return res
        .status(401)
        .json({ error: 'Email or password does not match' });
    }

    const { id, name } = client;

    return res.json({
      client: {
        id,
        name,
        email
      },
      token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expireIn
      })
    });
  }
}

export default new SessionClientController();
