import { Router } from 'express';

import SessionClientController from '../app/controllers/SessionClientController';
import SessionAdminController from '../app/controllers/SessionAdminController';

import authMiddleware from '../app/middlewares/middleAuth';

const routes = new Router();

routes.get('/', function Home(req, res) {
  res.send('Hello World!');
});
routes.post('/client', SessionClientController.store);
routes.post('/admin', SessionAdminController.store);

routes.use(authMiddleware);

routes.get('/dashboard', function Home(req, res) {
  res.send('Welcome to the dashboard!');
});
export default routes;
