import bcrypt from 'bcryptjs';

module.exports = {
  up: QueryInterface => {
    return QueryInterface.bulkInsert(
      'admins',
      [
        {
          name: 'Administrador',
          email: 'admin@admin.com',
          password_hash: bcrypt.hashSync('123456', 8),
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: () => {}
};
