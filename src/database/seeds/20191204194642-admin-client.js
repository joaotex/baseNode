import bcrypt from 'bcryptjs';

module.exports = {
  up: QueryInterface => {
    return QueryInterface.bulkInsert(
      'clients',
      [
        {
          name: 'João Marcos',
          email: 'joao.teixeira@gmail.com',
          password_hash: bcrypt.hashSync('123456', 8),
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: () => {}
};
