/* eslint-disable no-console */
import request from 'supertest';
import server from '../src/app';

import SessionClientController from '../src/app/controllers/SessionClientController';

const token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTgxMDg1NjQ3LCJleHAiOjE1ODExNzIwNDd9.Z_k8OWTxzWXiKR5KVkj3Kb83l2ix6N6Ni-qBh9i52eQ';

beforeAll(async () => {
  test('acessa a rota /cliente e testa o login do cliente', async () => {
    const response = await request(server)
      .post('/client', SessionClientController.store)
      .send({ email: 'joao.teixeira@gmail.com', password: '123456' });

    // .end((err, res) => {
    //   // token = res.body.token;
    //   done();
    // });

    expect(response.status).toEqual(200);
    expect(response.text).toContain(`${token}`);
  });
});

describe('GET /', () => {
  test('se o usuario não estiver logado se espera que ele de erro 401', async () => {
    const response = await request(server).get('/dashboard');
    expect(response.status).toEqual(401);
  });

  test('se o uruario estiver logado se espera que de retorne 200', async () => {
    const response = await request(server)
      .get('/dashboard')
      .set('Authorization', `Bearer ${token}`);
    expect(response.status).toBe(200);
  });
});

afterAll(() => {
  console.log('servidor fechado');
});
