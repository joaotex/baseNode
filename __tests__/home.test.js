/* eslint-disable no-console */
import request from 'supertest';
import server from '../src/app';

beforeAll(async () => {
  console.log('servidor iniciado');
});

describe('inicio dos testes', () => {
  test('acessa a rota da home e verifica o conteúdo que é exibido ', async () => {
    const response = await request(server).get('/');
    expect(response.status).toEqual(200);
    expect(response.text).toContain('Hello World!');
  });
});

afterAll(() => {
  console.log('servidor fechado');
});
